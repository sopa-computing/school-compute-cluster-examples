#!/bin/bash
#
#$ -cwd
#$ -N mcp_simple
#$ -m be

# Number of Monte Carlo trials to run. This can be passed as a
# command line argument, or will default to the number specified
num_trials=${1:-1000}

# Run the simulation, storing the final output in a file called
# result in the current directory
python3 monte_carlo_pi.py $num_trials -o result

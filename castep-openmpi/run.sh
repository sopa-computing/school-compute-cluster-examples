#!/bin/sh
#
# SGE options
# ===========
#
# The following options are pretty standard.
#$ -cwd
#$ -N castep_mpi
#$ -m beas
#
# The following options are required to run OpenMPI code on our cluster!
# The '-pe mpi 16' argument says we're going to be running MPI code over
# 16 cores. Change this number to suit.
#$ -pe mpi 16
#$ -R y
#
########################################################################

# When this script is run on the cluster, the special variable $NSLOTS
# will contain the value you passed in the '-pe mpi ...' option above.
# To also allow this script to be run outside the cluster, we create
# our own $np variable that is set to the value of $NSLOTS if it exists
# (i.e. if running on the cluster), otherwise 4 cores (i.e. if running
# outside the cluster).
np=${NSLOTS:-4}

# Load in environment modules so that we can run CASTEP 18.1 in parallel
# using OpenMPI 1.10
module load openmpi/1.10
module load castep/18.1

# Finally run the CASTEP simulation code
mpirun -np $np castep.mpi li

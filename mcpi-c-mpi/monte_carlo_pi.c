#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define SQUARE(x) (x)*(x)

static long do_monte_carlo(long num_trials);
static double next_random();

int main(int argc, char **argv) {
    long num_trials, success_count;
    double pi_estimate;

    // Read requested total number of trials from command line arguments
    if (argc != 2) {
        printf("Required argument: number of iterations\n");
        return EXIT_FAILURE;
    }
    num_trials = strtol(argv[1], NULL, 10);
    if (num_trials <= 0) {
        printf("Number of iterations must be a positive long integer\n");
        return EXIT_FAILURE;
    }

    // Run MC simulation
    success_count = do_monte_carlo(num_trials);

    // Calculate and display estimate of pi
    pi_estimate = 4.0 * success_count / num_trials;
    printf("%f %ld %ld\n", pi_estimate, success_count, num_trials);

    return EXIT_SUCCESS;
}

/**
 * Runs the Monte Carlo simulation for the given number
 * of trials, returning the number of successful samples.
 */
long do_monte_carlo(long num_trials) {
    double x,y;
    long i, success_count = 0;
    for (i = num_trials; i > 0; i--) {
        x = next_random();
        y = next_random();
        if (SQUARE(x-0.5) + SQUARE(y-0.5) < 0.25) {
            ++success_count;
        }
    }
    return success_count;
}

/**
 * Returns a pseudo-random number in the range [0,1).
 *
 * FIXME: You probably want to use a better random
 * number generator than this!
 */
double next_random() {
    return (double) rand() / RAND_MAX;
}

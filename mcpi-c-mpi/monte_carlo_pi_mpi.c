#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

#define SQUARE(x) (x)*(x)

static void merge_results_then_output(long master_success_count);
static long do_monte_carlo(long num_trials);
static double next_random();

int world_size;
int world_rank;
long num_trials_per_process;
long num_trials_total;

int main(int argc, char **argv) {
    long requested_trials, success_count;

    // Initialize the MPI environment
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Read requested total number of trials from command line arguments
    if (argc != 2) {
        printf("Required argument: number of trials\n");
        MPI_Finalize();
        return EXIT_FAILURE;
    }
    requested_trials = strtol(argv[1], NULL, 10);
    if (requested_trials <= 0) {
        printf("Number of trials must be a positive long integer\n");
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // Work out number of trials per process, increasing total
    // number of trials to be an integer multiple of this if required
    num_trials_per_process = (requested_trials + world_size - 1) / world_size;
    num_trials_total = num_trials_per_process * world_size;

    // Seed RNG to give different results for each rank
    srand(time(NULL) + world_rank);

    // Run MC simulation for this process
    success_count = do_monte_carlo(num_trials_per_process);

    if (world_rank == 0) {
        // This is the master process, so we'll merge this result
        // with the results received from the other processes
        merge_results_then_output(success_count);
    }
    else {
        // Send this result to master process
        MPI_Send(&success_count, 1, MPI_LONG, 0, 0, MPI_COMM_WORLD);
    }

    // Clean up and exit
    MPI_Finalize();
    return EXIT_SUCCESS;
}

/* Receives results from the slave processes and merges
 * them in with the result from the master process.
 *
 * The final result is them printed out
 */
void merge_results_then_output(long master_success_count) {
    long total_success_count = master_success_count;
    long count;
    int i;
    double pi_estimate;
    for (i = 1; i < world_size; i++) {
        MPI_Recv(&count, 1, MPI_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        total_success_count += count;
    }
    pi_estimate = 4.0 * total_success_count / num_trials_total;
    printf("%f %ld %ld\n", pi_estimate, total_success_count, num_trials_total);
}

/**
 * Runs the Monte Carlo simulation for the given number
 * of trials, returning the number of successful samples.
 */
long do_monte_carlo(long num_trials) {
    double x,y;
    long i, success_count = 0;
    for (i = num_trials; i > 0; i--) {
        x = next_random();
        y = next_random();
        if (SQUARE(x-0.5) + SQUARE(y-0.5) < 0.25) {
            ++success_count;
        }
    }
    return success_count;
}

/**
 * Returns a pseudo-random number in the range [0,1).
 *
 * FIXME: You probably want to use a better random
 * number generator than this!
 */
double next_random() {
    return (double) rand() / RAND_MAX;
}

#!/bin/sh
#
# The following arguments are pretty standard
#$ -cwd
#$ -N mcp_mpi
#$ -m beas
#
# The following arguments are required to run MPI code on our cluster!
# The '-pe mpi 16' argument says we're going to be running MPI code over
# 16 cores.
#$ -pe mpi 16
#$ -R y
#
########################################################################
 
# Number of Monte Carlo trials to run. This can be passed as a
# command line argument, or will default to the number specified
num_trials=${1:-1000000}

# The special variable $NSLOTS will contain the value you passed in the
# '-pe mpi ...' argument. To allow this script also to be run outside the cluster,
# we save this to a $np variable, defaulting to 4 cores if $NSLOTS is not set.
np=${NSLOTS:-4}

# We must first load in the OpenMPI shell module to set up the $PATH etc.
# This command loads in the current "best" supported version of OpenMPI.
# If you need to use a specific version of OpenMPI, use something like
# 'module load openmpi/1.10' instead. Type 'module available' to see what
# versions we currently support.
module load openmpi

# Now run the simulation code.
mpirun -np $np ./monte_carlo_pi_mpi $num_trials
